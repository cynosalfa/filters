﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Interfaces;
using Contracts.Attributes;
using System.ComponentModel.Composition;

namespace LinearFilter
{
    [Export(typeof(OneDimensionalFilter))]
    public partial class LowpassFilter : OneDimensionalFilter
    {        
        public override string GetFilterDescription()
        {
            return Description;
        }

        public override List<double> DoFilter(List<double> sourceArray)
        {
            var filteredArray = new List<double>();

            CalculateFilterWeight();

            for (int i = 0; i < sourceArray.Count(); i++)
            {
                double tempSum = 0;
                for (int j = 0; j < NumberOfSamples; j++)
                {
                    if ((i + j) < sourceArray.Count())
                    {
                        tempSum += _weight[j] * sourceArray[i + j];
                    }
                    else
                    {
                        tempSum += _weight[j];
                    }
                }

                filteredArray.Add(tempSum / _weightSum);
            }

            return filteredArray;
        }

        private void CalculateFilterWeight()
        {
            _weight = new List<double>();

            for (int i = 0; i < NumberOfSamples; i++)
            {
                _weight.Add(Math.Exp(-PassAmount * i));
            }

            _weightSum = _weight.Sum();
        }
    }
}