﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinearFilter
{
    public partial class LowpassFilter
    {
        public LowpassFilter()
        {
            Description = "LinearLowpassFilter";

            int numberOfSamplesValue;
            var numberOfSamplesConfig = ConfigurationManager.AppSettings["NumberOfSamples"];
            NumberOfSamples = numberOfSamplesConfig != null && 
                int.TryParse(numberOfSamplesConfig, out numberOfSamplesValue) ?
                numberOfSamplesValue :
                500;

            double passAmountValue;
            var passAmountConfig = ConfigurationManager.AppSettings["PassAmount"];
            PassAmount = passAmountConfig != null && 
                double.TryParse(passAmountConfig, out passAmountValue) ?
                passAmountValue :
                0.01;
        }
    }
}
