﻿using System.Collections.Generic;
using Contracts.Attributes;

namespace LinearFilter
{
    public partial class LowpassFilter
    {
        [PropertyDescription("Количество отсчётов", "ru-RU")]
        [PropertyDescription("Number of samples", "en-US", IsDefault = true)]
        public int NumberOfSamples { get; set; }

        [PropertyDescription("Коэффициент затухания экспоненты", "ru-RU")]
        [PropertyDescription("Pass amount", "en-US",IsDefault = true)]
        public double PassAmount { get; set; }

        private List<double> _weight;
        private double _weightSum;
    }
}