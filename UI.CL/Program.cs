﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using Contracts;
using Contracts.Interfaces;

namespace UI.CL
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputDirectoryName = String.Format("{0}\\{1}",
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Input");
            string outputDirectoryName = String.Format("{0}\\{1}",
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Output");

            if (!Directory.Exists(inputDirectoryName))
            {
                Console.WriteLine("Input directory \\Input not exists");
                Console.ReadKey();
                return;
            }

            if (!Directory.Exists(outputDirectoryName))
            {
                Directory.CreateDirectory(outputDirectoryName);
            }
            
            var importer = new Importer();

            importer.ImportFilters();
            var files = Directory.GetFiles(inputDirectoryName);

            List<List<double>> inputs = files
                    .Select(file => File.ReadAllLines(file).Select(double.Parse).ToList())
                    .ToList();

            foreach (var filter in importer.Filters)
            {
                var filterManager = new OneDimensionalFilterManager(filter.Value);

                try
                {
                    Console.WriteLine(filterManager.Filter.GetFilterDescription());                    

                    foreach(var prop in filterManager.Properties)
                    {
                        Console.WriteLine("{0} : {1}", prop.Description, prop.GetValue(filterManager.Filter));
                    }

                    for (int i = 0; i < inputs.Count(); i ++)
                    {
                        var result = filterManager.Filter.DoFilter(inputs[i]);

                        File.WriteAllLines(
                            String.Format("{0}\\{1}_{2}.txt", outputDirectoryName,
                                filterManager.Filter.GetFilterDescription(), i),
                            result.Select(res => res.ToString()));
                    }

                    Console.WriteLine("OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
           
            Console.ReadKey();
        }
    }
}
