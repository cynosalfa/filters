﻿using System;

namespace Contracts.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class PropertyDescriptionAttribute : Attribute
    {
        public string Description;
        public string Culture;
        public bool IsDefault;

        public PropertyDescriptionAttribute(string description, string culture)
        {
            Description = description;
            Culture = culture;
        }
    }
}