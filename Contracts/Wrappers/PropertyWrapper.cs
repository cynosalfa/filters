﻿using System.Linq;
using System.Reflection;
using System.Globalization;
using Contracts.Attributes;

namespace Contracts.Wrappers
{
    public class PropertyWrapper
    {
        private readonly MethodInfo _getter;
        private readonly MethodInfo _setter;
        private readonly string _description;

        public string Description { get { return _description; } }

        public PropertyWrapper(PropertyInfo property)
        {
            _getter = property.GetGetMethod();
            _setter = property.GetSetMethod();

            var descriptionAttributes = ((PropertyDescriptionAttribute[])property
                .GetCustomAttributes(typeof(PropertyDescriptionAttribute), true));

            if (descriptionAttributes.Any(attr => attr.Culture == CultureInfo.CurrentCulture.Name))
            {
                _description = descriptionAttributes.First(attr => attr.Culture == CultureInfo.CurrentCulture.Name).Description;
            }
            else
            {
                _description = descriptionAttributes.First(attr => attr.IsDefault).Description;
            }
        }

        public object GetValue(object instance)
        {
            return _getter.Invoke(instance, null);
        }

        public void SetValue(object instance, object value)
        {
            _setter.Invoke(instance, new object[] { value });
        }
    }
}
