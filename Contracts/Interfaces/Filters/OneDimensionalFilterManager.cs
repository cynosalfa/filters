﻿using System.Collections.Generic;
using System.Reflection;
using Contracts.Wrappers;

namespace Contracts.Interfaces
{
    public class OneDimensionalFilterManager
    {
        public OneDimensionalFilter Filter { get; set; }

        private readonly List<PropertyWrapper> _wrappedProperties;

        public List<PropertyWrapper> Properties
        {
            get { return _wrappedProperties; }
        }

        public OneDimensionalFilterManager(OneDimensionalFilter filter)
        {
            Filter = filter;
            PropertyInfo[] properties = filter.GetType().GetProperties();
            _wrappedProperties = new List<PropertyWrapper>();

            foreach (var prop in properties)
            {
                _wrappedProperties.Add(new PropertyWrapper(prop));
            }
        }
    }
}