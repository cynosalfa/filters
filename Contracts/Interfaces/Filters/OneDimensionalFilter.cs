﻿using System.Collections.Generic;

namespace Contracts.Interfaces
{
    public abstract class OneDimensionalFilter
    {
        protected string Description;

        public abstract string GetFilterDescription();
        public abstract List<double> DoFilter(List<double> sourceArray);

        public static string GetFilterDirectoryName()
        {
            return "Filters";
        }
    }
}