﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using Contracts.Interfaces;

namespace Contracts
{
    public class Importer
    {
        [ImportMany(typeof(OneDimensionalFilter))]
        private IEnumerable<Lazy<OneDimensionalFilter>> filters;

        public IEnumerable<Lazy<OneDimensionalFilter>> Filters
        {
            get
            {
                return filters;
            }
        }

        public void ImportFilters()
        {
            var catalog = new AggregateCatalog();

            catalog.Catalogs.Add(
                new DirectoryCatalog(
                    String.Format("{0}\\{1}", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                    OneDimensionalFilter.GetFilterDirectoryName())));

            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }
    }
}