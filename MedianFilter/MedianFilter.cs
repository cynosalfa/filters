﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Interfaces;
using System.ComponentModel.Composition;

namespace MedianFilter
{
    [Export(typeof(OneDimensionalFilter))]
    public partial class Filter : OneDimensionalFilter
    {        
        public override string GetFilterDescription()
        {
            return Description;
        }

        public override List<double> DoFilter(List<double> sourceArray)
        {
            if (sourceArray.Count() <= Aperture)
            {
                throw new Exception("Слишком большое значение апертуры медианного фильтра");
            }

            if (Aperture % 2 == 0)
            {
                throw new Exception("Апертура медианного фильтра должна быть нечётным числом");
            }

            //returning array
            var filteredArray = new List<double>();

            //filter processing structures
            var processingArray = new List<double>();
            var processingMatrix = new List<List<double>>();

            processingArray.AddRange(sourceArray);

            if (DuplicateEdgeValues)
            {
                DoDuplicateEdgeValues(ref processingArray);
            }

            for (int i = 0; i < processingArray.Count() - Aperture; i++)
            {
                var processingGroup = new List<double>();

                for (int j = 0; j < Aperture; j++)
                {
                    processingGroup.Add(processingArray[i + j]);
                }

                processingGroup.Sort();
                processingMatrix.Add(processingGroup);
            }

            for (int i = 0; i < processingMatrix.Count(); i++)
            {
                var filteredValue = processingMatrix[i][MedianIndex];

                if (UseCensor)
                {
                    if (DoCensor(filteredValue, sourceArray[i]))
                    {
                        filteredArray.Add(filteredValue);
                    }
                    else
                    {
                        filteredArray.Add(sourceArray[i]);
                    }
                }
                else
                {
                    filteredArray.Add(sourceArray[i]);
                }
            }

            return filteredArray;
        }

        private void DoDuplicateEdgeValues(ref List<double> input)
        {
            var preProcessedRow = new List<double>();
            preProcessedRow.AddRange(input);

            for (int i = 0; i < Aperture / 2; i++)
            {
                preProcessedRow.Insert(preProcessedRow.Count() - 1, input.Last());
                preProcessedRow.Insert(0, input[0]);
            }

            input = preProcessedRow;
        }

        private bool DoCensor(double filteredValue, double sourceValue)
        {
            return Math.Abs(filteredValue - sourceValue) > Epsilon;
        }
    }
}