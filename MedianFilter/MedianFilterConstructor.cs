﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedianFilter
{
    public partial class Filter
    {
        public Filter()
        {
            Description = "MedianFilter";

            int apertureValue;
            var apertureConfig = ConfigurationManager.AppSettings["Aperture"];
            Aperture = apertureConfig != null &&
                int.TryParse(apertureConfig, out apertureValue) ?
                apertureValue :
                3;

            double epsilonValue;
            var epsilonConfig = ConfigurationManager.AppSettings["Epsilon"];
            Epsilon = epsilonConfig != null &&
                double.TryParse(epsilonConfig, out epsilonValue) ?
                epsilonValue :
                0.1;

            bool useCensorValue;
            var useCensorConfig = ConfigurationManager.AppSettings["UseCensor"];
            UseCensor = useCensorConfig != null &&
                bool.TryParse(useCensorConfig, out useCensorValue) ?
                useCensorValue :
                true;

            bool duplicateEdgeValuesValue;
            var duplicateEdgeValuesConfig = ConfigurationManager.AppSettings["DuplicateEdgeValues"];
            DuplicateEdgeValues = duplicateEdgeValuesConfig != null &&
                bool.TryParse(duplicateEdgeValuesConfig, out duplicateEdgeValuesValue) ?
                duplicateEdgeValuesValue :
                false;
        }
    }
}
