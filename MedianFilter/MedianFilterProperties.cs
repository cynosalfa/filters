﻿using Contracts.Attributes;

namespace MedianFilter
{
    public partial class Filter
    {
        [PropertyDescription("Апертура", "ru-RU")]
        [PropertyDescription("Filter aperture", "en-US", IsDefault = true)]
        public int Aperture { get; set; }

        [PropertyDescription("Доверительный интервал", "ru-RU")]
        [PropertyDescription("Epsilon", "en-US", IsDefault = true)]
        public double Epsilon { get; set; }

        [PropertyDescription("Использовать цензурирование", "ru-RU")]
        [PropertyDescription("Need to use censor", "en-US", IsDefault = true)]
        public bool UseCensor { get; set; }

        [PropertyDescription("Дублировать краевые значения", "ru-RU")]
        [PropertyDescription("Need to duplicate edge values", "en-US", IsDefault = true)]
        public bool DuplicateEdgeValues { get; set; }

        private int MedianIndex
        {
            get { return Aperture / 2; }
        }


    }
}